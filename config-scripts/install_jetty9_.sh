#!/bin/bash


cd /tmp
wget http://download.eclipse.org/jetty/stable-9/dist/jetty-distribution-9.2.3.v20140905.tar.gz
tar xzvf jetty-distribution-9.2.3.v20140905.tar.gz

sudo mv jetty-distribution-9.2.3.v20140905 /opt/jetty
sudo rm jetty-distribution-9.2.3.v20140905.tar.gz

sudo rm -rf /opt/jetty/demo-base/

sudo cat <<EOT >> /etc/environment
JAVA_HOME=/usr/lib/jvm/java-7-oracle/
JETTY_HOME=/opt/jetty
JETTY_BASE=/opt/jetty/mybase

EOT

sudo useradd --user-group --shell /bin/false --home-dir /opt/jetty/temp jetty

cd /opt
sudo mkdir -p web/mybase/
sudo mkdir -p web/mybase/work
sudo mkdir jetty/temp/


cd /opt/jetty/lib/ext
wget http://central.maven.org/maven2/org/jboss/weld/servlet/weld-servlet/2.2.8.Final/weld-servlet-2.2.8.Final.jar
# wget http://central.maven.org/maven2/org/jboss/weld/servlet/weld-servlet/2.1.2.Final/weld-servlet-2.1.2.Final.jar

sudo chown -R jetty:jetty /opt/web
sudo chown -R jetty:jetty /opt/jetty

sudo usermod -a -G jetty user_name

sudo cp /opt/jetty/bin/jetty.sh /etc/init.d/jetty


sudo touch /etc/default/jetty


sudo cat <<EOT >> /etc/default/jetty
JETTY_HOME=/opt/jetty
JETTY_BASE=/opt/web/mybase
TMPDIR=/opt/jetty/temp

EOT

sudo service jetty start




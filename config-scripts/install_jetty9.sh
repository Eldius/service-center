#!/bin/bash


cd /tmp
wget http://eclipse.c3sl.ufpr.br/jetty/stable-9/dist/jetty-distribution-9.2.6.v20141205.tar.gz
tar xzvf jetty-distribution-9.2.6.v20141205.tar.gz

mv jetty-distribution-9.2.6.v20141205 /opt/jetty
rm jetty-distribution-9.2.6.v20141205.tar.gz

rm -rf /opt/jetty/demo-base/

cat <<EOT >> /etc/environment
JAVA_HOME=/usr/lib/jvm/java-7-oracle/
JETTY_HOME=/opt/jetty
JETTY_BASE=/opt/jetty/mybase

EOT

useradd --user-group --shell /bin/false --home-dir /opt/jetty/temp jetty

cd /opt
mkdir -p web/mybase/
mkdir -p web/mybase/work/
mkdir jetty/temp/

# usermod -a -G jetty user_name

cp /opt/jetty/bin/jetty.sh /etc/init.d/jetty

cp /vagrant/config-scripts/config-files/h2-1.4.184.jar /opt/jetty/lib/ext
cp /vagrant/config-scripts/config-files/transactions-osgi-3.9.3.jar /opt/jetty/lib/ext

touch /etc/default/jetty


cat <<EOT >> /etc/default/jetty
JETTY_HOME=/opt/jetty
JETTY_BASE=/opt/web/mybase
TMPDIR=/opt/jetty/temp

EOT

cp /opt/jetty/start.ini /opt/web/mybase/

cat <<EOT >> /opt/web/mybase/start.ini

--module=cdi

--module=logging

--module=debug

--module=monitor

--module=requestlog

--exec
-Xdebug
-agentlib:jdwp=transport=dt_socket,address=8585,server=y,suspend=n
EOT

cd /opt/web/mybase
echo y | java -jar /opt/jetty/start.jar --create-files

chown -R jetty:jetty /opt/web
chown -R jetty:jetty /opt/jetty



service jetty start




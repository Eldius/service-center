#!/bin/bash

mkdir ~/tmp
cd ~/tmp

wget http://download.jboss.org/jbossas/7.1/jboss-as-7.1.1.Final/jboss-as-7.1.1.Final.tar.gz

tar xfvz jboss-as-7.1.1.Final.tar.gz
mkdir /usr/local/share/jboss
mv jboss-as-7.1.1.Final /usr/local/share/jboss


/usr/local/share/jboss/jboss-as-7.1.1.Final/bin/add-user.sh --silent admin 123Senha

cp /vagrant/config-scripts/config-files/jbossas7 /etc/init.d/jbossas7 -f
cp /vagrant/config-scripts/config-files/jboss-as.conf /usr/local/share/jboss/jboss-as-7.1.1.Final/bin/init.d/ -f
cp /vagrant/config-scripts/config-files/standalone.xml /usr/local/share/jboss/jboss-as-7.1.1.Final/standalone/configuration/ -f


## Start when entering to multi-user mode.
ln -s /etc/init.d/jbossas7 /etc/rc3.d/S84jbossas7
ln -s /etc/init.d/jbossas7 /etc/rc5.d/S84jbossas7
ln -s /etc/init.d/jbossas7 /etc/rc4.d/S84jbossas7

## Kill on own or when switching to single-user mode.
ln -s /etc/init.d/jbossas7 /etc/rc6.d/K15jbossas7
ln -s /etc/init.d/jbossas7 /etc/rc0.d/K15jbossas7
ln -s /etc/init.d/jbossas7 /etc/rc1.d/K15jbossas7
ln -s /etc/init.d/jbossas7 /etc/rc2.d/K15jbossas7


sudo update-rc.d jbossas7 defaults

ln -s /lib/lsb/init-functions /etc/init.d/functions



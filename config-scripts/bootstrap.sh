#!/bin/bash

#http://repogen.simplylinux.ch/
cp /etc/apt/sources.list /etc/apt/sources.list.bkp
cp /vagrant/config-scripts/config-files/sources.list /etc/apt/sources.list


mkdir tmp
cd tmp

apt-get update
apt-get install -y python-software-properties
add-apt-repository ppa:webupd8team/java
apt-get update

# confirmacao para o Oracle JDK
echo debconf shared/accepted-oracle-license-v1-1 select true | \
debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | \
debconf-set-selections

echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | \
/usr/bin/debconf-set-selections

apt-get install -y oracle-java7-installer

apt-get install -y curl joe maven

/vagrant/config-scripts/install_nginx.sh

/vagrant/config-scripts/install_jetty9.sh

echo Fim...

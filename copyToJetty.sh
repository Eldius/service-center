#!/bin/bash
sudo service jetty stop
sudo cp /vagrant/service-center-rest/target/*.war /opt/web/mybase/webapps

echo Server log files...
sudo rm /opt/jetty/logs/*.*

echo App log files...
echo "Emtpy log files:"
for f in /opt/web/mybase/logs/*.*
do
 echo "Processing $f..."
 sudo rm $f
done

echo Temp files...
sudo rm -rf /opt/web/mybase/work/**
ls -la /opt/web/mybase/work/
sudo service jetty start



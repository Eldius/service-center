set PROJECT_HOME=%CD%

cls

call set "MAVEN_OPTS="


cd %PROJECT_HOME%\service-center-web
call mvn clean wildfly:deploy -DskipTests=true

call set "MAVEN_OPTS="

cd %PROJECT_HOME%

msg * Build finished!

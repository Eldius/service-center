package net.eldiosantos.rest.factory;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * Created by Eldius on 31/12/2014.
 */
public class EntityManagerProducer {
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("default");

    @Produces
    @RequestScoped
    public EntityManager criaEntityManager() {
        EntityManager entityManager = factory.createEntityManager();
        return entityManager;
    }

    public void finaliza(@Disposes EntityManager manager) {
        manager.close();
    }
}

package net.eldiosantos.rest.aop.transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Created by Eldius on 02/01/2015.
 */
@Interceptor
@Transactional
public class TransactionInterceptor {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Inject
    private EntityManager entityManager;

    @AroundInvoke
    public Object execute(final InvocationContext context) throws Exception {

        logger.debug("About to open transaction...");
        final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        logger.debug("Transaction open.");

        try {
            logger.debug("Executing method...");
            final Object result = context.proceed();
            logger.debug("Method execution completed...");

            logger.debug("About to commit transaction...");
            if (transaction.isActive()) {
                transaction.commit();
                logger.debug("Transaction commited.");
            } else {
                logger.debug("Transaction is not active.");
            }
            return result;
        } catch (Exception e) {
            if (transaction.isActive()) {
                transaction.rollback();
                logger.debug("Transaction rolled back.");
            }
            logger.error("Error", e);
            throw e;
        }
    }
}

package net.eldiosantos.rest.aop.transaction;

import javax.interceptor.InterceptorBinding;
import java.lang.annotation.*;

/**
 * Created by Eldius on 02/01/2015.
 */
@Inherited
@InterceptorBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface Transactional {
}

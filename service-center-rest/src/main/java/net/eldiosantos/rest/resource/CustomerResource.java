package net.eldiosantos.rest.resource;

import net.eldiosantos.rest.aop.transaction.Transactional;
import net.eldiosantos.servicecenter.model.Customer;
import net.eldiosantos.servicecenter.qualifier.Hibernate;
import net.eldiosantos.servicecenter.repository.CustomerRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

/**
 * Created by Eldius on 30/12/2014.
 */
@Path("customer")
public class CustomerResource {

    @Inject
    @Hibernate
    private CustomerRepository customerRepository;

    @Path("/{customerId}")
    @GET
    public Response get(@PathParam("customerId") final Long customerId) {
        return Response.ok(customerRepository.getByPk(customerId)).build();
    }

    @Path("/")
    @GET
    public Response list() {
        return Response.ok(customerRepository.listAll()).build();
    }

    @Path("/")
    @POST
    @Transactional
    public Response save(final Customer customer) {
        if(customer.getId() == null) {
            customerRepository.persist(customer);
        } else {
            customerRepository.update(customer);
        }

        return Response.ok(customer).build();
    }

    @Path("/{customerId}")
    @DELETE
    public Response delete(@PathParam("customerId") final Long customerId) {
        Customer c = customerRepository.getByPk(customerId);
        customerRepository.delete(c);
        return Response.ok("ok").build();
    }
}

set PROJECT_HOME=%CD%

cls

call set "MAVEN_OPTS="

call mvn clean install -DskipTests=true

cd %PROJECT_HOME%\service-center-web
call mvn wildfly:run -DskipTests=true

call set "MAVEN_OPTS="

cd %PROJECT_HOME%


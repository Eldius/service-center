package net.eldiosantos.servicecenter.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

	private Customer customer = null;

	@Before
	public void setUp() throws Exception {
		customer = new Customer();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetFirstName() {
		customer.setFirstName("Kenny");
		customer.setLastName("McCormick");
		customer.setAddress("South Park, Colorado");
		customer.setEmail("kenny@mailinator.com");

		assertEquals("Same name", "Kenny", customer.getFirstName());
	}

	@Test
	public void testGetLastName() {
		customer.setFirstName("Kenny");
		customer.setLastName("McCormick");
		customer.setAddress("South Park, Colorado");
		customer.setEmail("kenny@mailinator.com");

		assertEquals("Same last name", "McCormick", customer.getLastName());
	}

	@Test
	public void testGetEmail() {
		customer.setFirstName("Kenny");
		customer.setLastName("McCormick");
		customer.setAddress("South Park, Colorado");
		customer.setEmail("kenny@mailinator.com");

		assertEquals("Same email", "kenny@mailinator.com", customer.getEmail());
	}

	@Test
	public void testGetAddress() {
		customer.setFirstName("Kenny");
		customer.setLastName("McCormick");
		customer.setAddress("South Park, Colorado");
		customer.setEmail("kenny@mailinator.com");

		assertEquals("Same address", "South Park, Colorado", customer.getAddress());
	}

	@Test
	public void testGetFirstNameNotNull() {
		assertNotNull("Get first name doesn't return null", customer.getFirstName());
	}

	@Test
	public void testGetLastNameNotNull() {
		assertNotNull("Get last name doesn't return null", customer.getLastName());
	}

	@Test
	public void testGetEmailNotNull() {
		assertNotNull("Get email doesn't return null", customer.getEmail());
	}

	@Test
	public void testGetAddressNotNull() {
		assertNotNull("Get address doesn't return null", customer.getAddress());
	}

	@Test
	public void testGetFullNameNotNull() {
		assertNotNull("Get full name doesn't return null", customer.getLastName());
	}

	@Test
	public void testValidFullName() {
		final String firstName = "Kenny";
		final String lastName = "McCormick";

		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setAddress("South Park, Colorado");
		customer.setEmail("kenny@mailinator.com");

		assertEquals("Full name have a space between first and last name", firstName + " " + lastName, customer.getFullName());

	}

	@Test
	public void testToString() {
		final String firstName = "Kenny";
		final String lastName = "McCormick";

		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setAddress("South Park, Colorado");
		customer.setEmail("kenny@mailinator.com");

		final String string = customer.toString();
		assertTrue("To String contains the first name", string.contains(firstName));
		assertTrue("To String contains the last name", string.contains(firstName));
	}

}

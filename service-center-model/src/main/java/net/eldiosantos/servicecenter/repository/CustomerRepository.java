package net.eldiosantos.servicecenter.repository;

import net.eldiosantos.servicecenter.model.Customer;
import net.eldiosantos.servicecenter.repository.interfaces.Repository;

public interface CustomerRepository extends Repository<Customer, Long> {

}

package net.eldiosantos.servicecenter.model;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Customer implements Serializable, Comparable<Customer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4987422133714712288L;

	private Long id;

	private String firstName;
	private String lastName;
	private String email;
	private String address;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName!=null?firstName:"";
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName!=null?lastName:"";
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email!=null?email:"";
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address!=null?address:"";
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFullName() {
		return new StringBuffer(this.getFirstName())
			.append(" ")
			.append(this.getLastName())
			.toString();
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false);
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, false);
	}

	@Override
	public int compareTo(Customer other) {
		if(other!=null) {
			return this.getFullName().compareToIgnoreCase(other.getFullName());
		} else {
			return 1;
		}
	}

}

create table LOG_ENTRY (
	id NUMBER(9) NOT NULL AUTO_INCREMENT
	, category VARCHAR(255)
	, thread VARCHAR(255)
	, logdate TIMESTAMP
	, location VARCHAR(255)
	, throwable CLOB
	, message CLOB
	, PRIMARY KEY (id)
);
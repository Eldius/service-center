package net.eldiosantos.servicecenter.repository.hibernate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.List;

import net.eldiosantos.servicecenter.JpaBaseTestClass;
import net.eldiosantos.servicecenter.model.Customer;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CustomerRepositoryHibernateImplTest extends JpaBaseTestClass {

	private CustomerRepositoryHibernateImpl repository;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		logger.debug("CustomerRepositoryTest.setUpBeforeClass");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		logger.debug("CustomerRepositoryTest.tearDownAfterClass");
	}

	@Before
	public void setUp() throws Exception {
		logger.debug("CustomerRepositoryTest.setUp");
		repository = new CustomerRepositoryHibernateImpl(em);
	}

	@After
	public void tearDown() throws Exception {
		logger.debug("CustomerRepositoryTest.tearDown");
	}

	@Test
	public void testInsertElement() {
		em.getTransaction().begin();

		Customer c = new Customer();
		c.setFirstName("Fulano");
		c.setLastName("de Tal");
		c.setEmail("fulano.de.tal@mailinator.com");
		c.setAddress("69, No Name st, Anywhere");
		
		repository.persist(c);

		em.getTransaction().commit();

		assertNotNull("Primary key is not null", c.getId());
	}

	@Test
	public void testGetByPkNonExistent() {
		Customer customer = repository.getByPk(1l);
		assertNull("Trying to get a customer with nonexistent id", customer);
	}

	@Test
	public void testGetByPk() {
		Customer customer = repository.getByPk(5l);
		assertNotNull("Trying to get a customer", customer);
	}

	@Test
	public void testListAll() {
		List<Customer> list = repository.listAll();
		assertFalse("Returning list isn't empty", list.isEmpty());
		assertEquals("Returning all six customers from database", 6, list.size());
	}

	@Test
	public void testPersist() {
		em.getTransaction().begin();

		Customer c = repository.getByPk(5l);
		String firstName = "Mriazinha";
		c.setFirstName(firstName);
		String lastName = "da Silva";
		c.setLastName(lastName);
		String email = "mariazinha@mailinator.com";
		c.setEmail(email);
		String address = "69, No Name st, Anywhere";

		repository.persist(c);

		em.getTransaction().commit();

		assertNotNull("Primary key is not null", c.getId());

		Customer c1 = repository.getByPk(5l);

		assertEquals("First name was modified", firstName, c1.getFirstName());
		assertEquals("Last name was modified", lastName, c1.getLastName());
		assertEquals("Email was modified", email, c1.getEmail());
		assertEquals("Address wasn't modified", address, c1.getAddress());

	}

	@Test
	public void testDelete() {
		em.getTransaction().begin();

		Customer c = repository.getByPk(5l);
		repository.delete(c);

		em.getTransaction().commit();

		List<Customer> list = repository.listAll();

		assertFalse("Element was removed", list.contains(c));
		assertEquals("List size was changed", 5 ,list.size());
	}

	@Override
	public String getPersistenceUnit() {
		return "default";
	}

	@Override
	public List<String> getScriptFile() {
		return Arrays.asList("inserts.sql");
	}
}

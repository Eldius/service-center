package net.eldiosantos.servicecenter;

import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class JpaBaseTestClass {

	protected static final Logger logger = LoggerFactory.getLogger(JpaBaseTestClass.class);

	protected static EntityManager em;
	private static EntityManagerFactory emf;

	@BeforeClass
	public static void setUpBeforeClassSuper() throws Exception {
		logger.debug("JpaBaseTestClass.setUpBeforeClass");
		emf = Persistence.createEntityManagerFactory("default");
	}

	@Before
	public void setUpSuper() throws Exception {
		logger.debug("JpaBaseTestClass.setUp");
		em = emf.createEntityManager();
		executeScripts();
	}

	private void executeScripts() {
		List<String> scriptFileNames = getScriptFile();
		for(String scriptFileName:scriptFileNames) {
			em.getTransaction().begin();
			if((scriptFileName!=null) && (!scriptFileName.isEmpty())) {
				Query query = em.createNativeQuery(getScriptFromFile(scriptFileName));
				try {
					logger.info("script executed: " + query.executeUpdate());
				} catch (Exception e) {
					logger.error("Error while executing pre test script " + scriptFileName, e);
				}
			}
			em.getTransaction().commit();
		}
	}

	@After
	public void tearDownSuper() throws Exception {
		logger.debug("JpaBaseTestClass.tearDown");
		em.close();
	}

	public abstract String getPersistenceUnit();
	public abstract List<String> getScriptFile();

	private String getScriptFromFile(final String fileName) {

		final StringBuffer sql = new StringBuffer();
		final InputStream file = this.getClass().getClassLoader().getResourceAsStream(fileName);
		final Scanner scanner = new Scanner(file);

		while(scanner.hasNext()) {
			sql.append(scanner.nextLine()).append("\n");
		}

		scanner.close();
		return sql.toString();
	}
}

delete from CUSTOMER where ID > 0;

INSERT INTO CUSTOMER (ID,ADDRESS,EMAIL,FIRST_NAME,LAST_NAME) VALUES (5,'69, No Name st, Anywhere','fulano.de.tal@mailinator.com','Fulano','de Tal');
INSERT INTO CUSTOMER (ID,ADDRESS,EMAIL,FIRST_NAME,LAST_NAME) VALUES (6,'171, No Name st, Anywhere','fulano.de.tal@mailinator.com','Fulano','De Tal II');
INSERT INTO CUSTOMER (ID,ADDRESS,EMAIL,FIRST_NAME,LAST_NAME) VALUES (7,'171, No Name st, Anywhere','homer@mailinator.com','Homer','J. Simpson');
INSERT INTO CUSTOMER (ID,ADDRESS,EMAIL,FIRST_NAME,LAST_NAME) VALUES (8,'24, 9th Street, South Park Colorado','eric@mailinator.com','Eric','Cartman');
INSERT INTO CUSTOMER (ID,ADDRESS,EMAIL,FIRST_NAME,LAST_NAME) VALUES (9,'13, 9th Street, South Park Colorado','kyle@mailinator.com','Kyle','Broflovski');
INSERT INTO CUSTOMER (ID,ADDRESS,EMAIL,FIRST_NAME,LAST_NAME) VALUES (10,'17, 9th Street, South Park Colorado','stan@mailinator.com','Stan','Marsh');


package net.eldiosantos.servicecenter;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import net.eldiosantos.servicecenter.model.Customer;

public class App {

	private EntityManager em;
	private EntityManagerFactory emf;

	public void initEmfAndEm() {
		emf = Persistence.createEntityManagerFactory("default");
		em = emf.createEntityManager();
	}

	public void cleanup() {
		em.close();
	}

	public static void main(String[] args) {
		new App().test();
	}

	public void test() {
		
		initEmfAndEm();
		
		Customer c = new Customer();
		
		c.setFirstName("Fulano");
		c.setLastName("de Tal");
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();

		cleanup();
	}
}

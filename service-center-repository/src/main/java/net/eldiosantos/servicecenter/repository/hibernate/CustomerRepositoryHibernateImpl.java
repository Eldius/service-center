package net.eldiosantos.servicecenter.repository.hibernate;

import javax.persistence.EntityManager;

import net.eldiosantos.servicecenter.model.Customer;
import net.eldiosantos.servicecenter.qualifier.Hibernate;
import net.eldiosantos.servicecenter.repository.CustomerRepository;
import net.eldiosantos.servicecenter.repository.hibernate.base.BaseRepository;

@Hibernate
public class CustomerRepositoryHibernateImpl extends BaseRepository<Customer, Long> implements CustomerRepository {

	@Deprecated
	public CustomerRepositoryHibernateImpl(){
	}

	public CustomerRepositoryHibernateImpl(EntityManager entityManager) {
		super(entityManager);
	}
}

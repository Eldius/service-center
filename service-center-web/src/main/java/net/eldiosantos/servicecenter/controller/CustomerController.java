package net.eldiosantos.servicecenter.controller;

import java.util.List;

import javax.inject.Inject;

import net.eldiosantos.servicecenter.model.Customer;
import net.eldiosantos.servicecenter.repository.CustomerRepository;
import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.view.Results;

@Controller
@Path("/customers")
public class CustomerController {

	@Inject
	private CustomerRepository customerRepository;

	@Inject
	private Result result;

	@Deprecated
	public CustomerController() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomerController(CustomerRepository customerRepository,
			Result result) {
		super();
		this.customerRepository = customerRepository;
		this.result = result;
	}

	@Path("/")
	public List<Customer>index() {
		return customerRepository.listAll();
	}

	@Path("/save")
	public void save(Customer customer) {
		if(customer.getId() != null) {
			customerRepository.update(customer);
		} else {
			customerRepository.persist(customer);
		}
		result.redirectTo(this).index();
	}

	@Path("/edit/{customer.id}")
	public void edit(Customer customer) {
		customer = customerRepository.getByPk(customer.getId());
		result.include("customer", customer);
		result.forwardTo(this).index();
	}

	@Path("/get/{customerId}/json")
	public void findAsJson(Long customerId) {
		final Customer customer = customerRepository.getByPk(customerId);
		result.use(Results.json()).withoutRoot().from(customer).serialize();
	}

	@Path("/delete/{customerId}")
	public void delete(Long customerId) {
		final Customer customer = customerRepository.getByPk(customerId);
		customerRepository.delete(customer);

		result.redirectTo(this).index();
	}
}

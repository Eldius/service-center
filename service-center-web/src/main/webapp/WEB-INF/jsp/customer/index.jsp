
<h3><fmt:message key="form.customer.title" /></h3>

<button class="btn btn-default" onclick="showModalForm();">Novo</button>

<%@ include file="/ui_components/customer/customer_table.jsp" %>


<br />

<%@ include file="/ui_components/customer/customer_modalform.jsp" %>

<script>
	function init(){
		$(".menu").find(".customer").addClass("active");

		$(".editElement").click(function(){
			var url = $(this).data('element-url');
			showModalForm(url);
		});
	}
</script>
